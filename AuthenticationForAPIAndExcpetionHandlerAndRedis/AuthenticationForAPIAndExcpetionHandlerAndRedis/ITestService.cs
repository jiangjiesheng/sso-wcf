﻿using com.jiangjiesheng.auth.Constant;
using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace com.jiangjiesheng.auth
{
    // 注意: 使用“重构”菜单上的“重命名”命令，可以同时更改代码和配置文件中的接口名“IService1”。
    [ServiceContract]
    public interface ITestService
    {   // 后期继续查为什么Description不能正确显示，变成 Service at http://localhost:23322/TestServiceImpl.svc/Test/Auth/{PARAM1}/{PARAM2}
        //WebInvoke也可以换成WebGet

        [OperationContract(Name = "TestIgnoreAuth")]
        [Description("忽略认证")]
        [WebInvoke(Method = "GET", UriTemplate = "Test/IgnoreAuth", RequestFormat = ServiceEnvironment.WebRequestFormatJson, ResponseFormat = ServiceEnvironment.WebResponseFormatJson)]
        ServerResponse<String> TestIgnoreAuth();

        [OperationContract(Name = "TestAuth")]
        [Description("需要认证")]
        [WebInvoke(Method = "GET", UriTemplate = "Test/Auth/{Param1}/{Param2}", ResponseFormat = ServiceEnvironment.WebResponseFormatJson, RequestFormat = ServiceEnvironment.WebRequestFormatJson)]
        ServerResponse<String> TestAuth(String Param1, String Param2);

        [OperationContract(Name = "TestLogin")]
        [Description("登录-忽略认证")]
        [WebInvoke(Method = "GET", UriTemplate = "Test/Login/{userId=88888}", RequestFormat = ServiceEnvironment.WebRequestFormatJson, ResponseFormat = ServiceEnvironment.WebResponseFormatJson)]
        ServerResponse<AppUserInfoEntity> TestLogin(String userId);

        [OperationContract(Name = "TestLogout")]
        [Description("用户注销 UserPhone AccessToken")]
        [WebInvoke(Method = "GET", UriTemplate = "Test/Logout", RequestFormat = ServiceEnvironment.WebRequestFormatJson, ResponseFormat = ServiceEnvironment.WebResponseFormatJson)]
        ServerResponse<String> TestLogout();
 
    }

     
}
