﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;

namespace com.jiangjiesheng.auth.Interceptors
{
    public class MessageInspectorBehavior : IEndpointBehavior
    {
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {

        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {

            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new AuthInterceptor());

        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }
        //  public override Type BehaviorType  
        //{  
        //    get { return typeof(LoginedInterceptor); }  
        //}  
        //protected override object CreateBehavior()  
        //{  
        //    return new LoginedInterceptor();  
        //}  
        //#region IEndpointBehavior Members  
        //public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)  
        //{  
        //}  
        //public void ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)  
        //{  
        //    clientRuntime.MessageInspectors.Add(new LoginedInterceptor());  
        //}  
        //public void ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)  
        //{  
        //}  
        //public void Validate(ServiceEndpoint endpoint)  
        //{  
        //}  
        //#endregion  
    }

}
