﻿
using com.jiangjiesheng.auth.Constant;
using com.jiangjiesheng.auth.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text.RegularExpressions;
using System.Web;
 

namespace com.jiangjiesheng.auth.Interceptors
{//IDispatchMessageInspector IClientMessageInspector  

    /**
     * 
     * 20180429 拦截器
     * 江节胜 dev@jiangjiesheng.cn
     * 
     */
    public class AuthInterceptor : IDispatchMessageInspector
    {
           
        String  AuthConfigPath = JSConfigTool.getAppDataConfigPath(JSConfigTool.DataConfigPath.AuthConfig);
       
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {

            if (HttpContext.Current.Request.HttpMethod.Equals("OPTIONS")) //POST的预请求,如果不添加该段代码，就需要在接口的入口中添加GetOptions(),详见《C#-积累-配置跨域访问-post报405-OPTIONS请求-WCF框架报错-需要json或xml，不能为raw-nginx反向代理配置文件》
            { //此时的响应头取决于web.config
                return true;
            }

            bool isOpenAuth = Boolean.Parse(JSConfigTool.getAppDataConfig(AuthConfigPath, Keys.ConfigIsOpenAuth));
            if (!isOpenAuth)//关闭验证
            {
                return true;
            }

            HttpRequest currentRequest = HttpContext.Current.Request;
            String rawUrl = currentRequest.RawUrl;
            if (!isSkipCheck(rawUrl))
            {   
                // return ResponseStatus.getCode(Status.NEED_LOGIN);

                // throw new FaultException(string.Format("Exception accessing database:{0}",
                //"ceshi "), new FaultCode("Connect to database"));

                //获取登录数据 然后 放行 ，如果实在获取不到action 方法名 ，就通过参数来数据

     
                var accessId = RequestUtil.GetHeaderAccessId();
                var accessToken = RequestUtil.GetHeaderAccessToken();


                //20180803新增直接从cookies中获取 由于此框架的redis保存的key是手机号，所以cookie中也需要上传accessid, accessToken
                var accessTokenCookie =  CookiesUtil.GetCookie(Keys.KEY_COOKIES_USER_ACCESSID_PREFIX);


                if ((String.IsNullOrWhiteSpace(accessToken) || String.IsNullOrWhiteSpace(accessId)) && String.IsNullOrWhiteSpace(accessTokenCookie))
                {
                    ServerResponse<String> r = ServerResponse<String>.create(ResponseStatus.getCode(Status.NO_VAILD_HEADER_AND_COOKIE), ResponseStatus.getDesc(Status.NO_VAILD_HEADER_AND_COOKIE), "");
                    this.ReplyAndAbortRequest(r, channel, instanceContext);
                    return false;
                }
                ServerResponse<AppUserInfoEntity> query = UserInfoCache.getUserInfo();//accessId, accessToken
                bool isLogined = query.IsSuccess();
                if (!isLogined)
                {
                    ServerResponse<String> r = ServerResponse<String>.create(ResponseStatus.getCode(Status.NEED_LOGIN), query.message, "");
                    this.ReplyAndAbortRequest(r, channel, instanceContext);
                    return false;
                }
                //刷新有效时间放在getUserInfo() 中了
                return false;
            }
            return true;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)// //AfterReceiveRequest 的返回值 将在BeforeSendReply 中 correlationState 上读取
        {
            Message returnV = reply;
            Console.WriteLine(reply.ToString());
        }


        private string GetHeaderValue(string name, string ns = "http://tempuri.org")
        {
            var RequestMessage = OperationContext.Current.RequestContext.RequestMessage;
            var headers = OperationContext.Current.IncomingMessageHeaders;
            var index = headers.FindHeader(name, ns);
            if (index > -1)
                return headers.GetHeader<string>(index);
            else
                return null;
        }

        private void ReplyAndAbortRequest<T>(T t, IClientChannel channel, InstanceContext instanceContext) //
        {
            HttpContext.Current.Response.AddHeader("Content-Type", "application/json");//实测两个都可以
            HttpContext.Current.Response.ContentType = "application/json";//实测两个都可以
            HttpContext.Current.Response.Clear();

            HttpContext.Current.Response.AddHeader("Content-Length", "");//去掉Transfer-Encoding:chunked 头必须 但是某些情况导致请求不能中断,填写具体值又会导致数据被丢失
            HttpContext.Current.Response.Write(JsonHelper.ObjectToJsonNotForSerialize(t));
            HttpContext.Current.Response.Flush();
            // HttpContext.Current.Response.End(); //20180730 实测发布iis服务中时这个导致不能终止，第二次无法请求，甚至无法发布成功
            HttpContext.Current.Response.Close();//这里处理结束后不要在BeforeSendReply()中继续调用
            instanceContext.Abort();//起关键作用 中断
            channel.Abort();
            channel.Close();
            HttpContext.Current.Request.Abort();

        }

        private bool isSkipCheck(String rawUrl)
        {
            // rawUrl = /TestServiceImpl.svc/user/logining-status 

            if (string.IsNullOrEmpty(rawUrl))
            {
                return false;
            }
            String authIgnorePathStr = JSConfigTool.getAppDataConfig(AuthConfigPath, "authConfig/authIgnorePath");
            authIgnorePathStr = authIgnorePathStr.Replace("\\r\\n", "").Replace("\\\"", "\"").Replace("\r\n", "").Replace(" ", "");

            string[] arr = Regex.Split(authIgnorePathStr, "###", RegexOptions.IgnoreCase);
            foreach (var url in arr)
            {
                if (String.IsNullOrWhiteSpace(url))//最后一个是空
                {
                    continue;

                }
                //if (rawUrl.IndexOf(url) > -1) //这个算法 导致 login(忽略) 和 login-status(不忽略) 被误判
                //{
                //    return true;
                //}

                if (rawUrl.ToLower().Equals(url.ToLower()) || rawUrl.Substring(0, rawUrl.Length-1).ToLower().Equals(url.ToLower())) // 实测有时请求后会被添加一个/
                {
                    return true;
                }
                if (url.IndexOf("***") > 0)//***表示是Restful风格的Get请求，也能起到通配符的作用
                {
                    String subUrl = url.Substring(0, url.IndexOf("***") - 1);
                    if (rawUrl.IndexOf(subUrl) > -1)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
