﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Web;

namespace com.jiangjiesheng.auth.Interceptors
{
    public class MessageInspectorExtensionElement : BehaviorExtensionElement
    {
        public override Type BehaviorType
        {
            get { return typeof(MessageInspectorBehavior); }
        }

        protected override object CreateBehavior()
        {
            return new MessageInspectorBehavior();
        }
    }
}