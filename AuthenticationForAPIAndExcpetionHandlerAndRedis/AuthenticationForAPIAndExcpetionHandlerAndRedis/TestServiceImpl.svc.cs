﻿
using AuthenticationForAPIAndExcpetionHandlerAndRedis.DAO.User;
using com.jiangjiesheng.auth.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace com.jiangjiesheng.auth
{
    // 注意: 使用“重构”菜单上的“重命名”命令，可以同时更改代码、svc 和配置文件中的类名“Service1”。
    // 注意: 为了启动 WCF 测试客户端以测试此服务，请在解决方案资源管理器中选择 Service1.svc 或 Service1.svc.cs，然后开始调试。
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    [WCF_ExceptionBehaviour(typeof(WCF_ExceptionHandler))]
    public class TestServiceImpl : ITestService
    {
        public ServerResponse<String> TestIgnoreAuth()
        {
            return ServerResponse<String>.create(0, "请求成功啦", null);
        }
        public ServerResponse<String> TestAuth(String Param1, String Param2)
        {
            return ServerResponse<String>.create(0, "请求成功啦", "Param1=" + Param1 + ",Param2=" + Param2);
        }

        public ServerResponse<AppUserInfoEntity> TestLogin(String userId)
        {

            ServerResponse<AppUserInfoEntity> user = UserDao.GetAppUserInfoByUserId(userId); //应该在IUserService接口中调用
            ServerResponse<String> save = UserInfoCache.setOrUpdateUserInfo(user.GetResult());
            if (save.IsSuccess())
            {
                user.GetResult().AccessToken = save.GetResult();//后期如果保存失败要禁止返回信息
                return ServerResponse<AppUserInfoEntity>.create(0, "登录成功,看看能不能调用需要认证接口", user.GetResult());
            }
            else//保存到redis服务时失败了，这时候不应该返回正常状态
            {
                return ServerResponse<AppUserInfoEntity>.create(-1, "登录失败：" + save.GetMessage(), user.GetResult());
            }

        }

        public ServerResponse<String> TestLogout()
        {
            return UserInfoCache.removeUserInfo();//应该在IUserService接口中调用
        }

    }
}
