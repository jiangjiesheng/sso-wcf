﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

/**
 * 
 * ServerResponse工具类说明：
 * 
 * 对所有接口返回值做一次封装
 * 例如：
 * {
    "code": -1,
    "message": "请求成功",
    "result": {
        "PassWord": "2222",
        "UserName": "1144"
    }
}
 * 
 * 生成xml如果有两个命名空间,会导致不好解析。
 * 解决方案见 《测试数据及截图说明》(相应实体的命名空间传"")
 * 
 * 添加引用-->程序集--> 搜索 serialization --> system.runtime.serialization 支持序列化显示字段
 * 
 * @author： 江节胜 dev@jiangjiesheng.cn
 * @date：20171026 1540
 * 
 **/

namespace com.jiangjiesheng.auth
{
    [DataContract]

    public class ServerResponse<T>
    {

        [DataMember]
        public Int32 code;
        [DataMember]
        public string message;
        [DataMember]
        public T result;// 如果是data,结果会按字母升序排序
        public ServerResponse()
        {

        }
        public ServerResponse(int code, string message, T result)
        {
            this.code = code;
            this.message = message;
            this.result = result;
        }

        public static ServerResponse<T> create(int code, string message, T result)
        {
            return new ServerResponse<T>(code, message, result); ;
        }

        public bool IsSuccess()
        {
            return this.code == 0;//或者可应用Const.ResponseCode.SUCCESS
        }
        public int GetCode()
        {
            return code;

        }
        public string GetMessage()
        {
            return message;
        }
        public T GetResult()
        {
            return result;

        }
        public void SetMessage(String msg)
        {
            this.message = msg;
        }
    }

}