﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace com.jiangjiesheng.auth
{
    [DataContract]
    public class AppUserInfoEntity
    {
        public AppUserInfoEntity() { }

        [DataMember]
        public String UserId { get; set; }

        [DataMember]
        public String AccessToken { get; set; }

        public AppUserInfoEntity(String UserId)
        {
            this.UserId = UserId;
        }
    }
}
