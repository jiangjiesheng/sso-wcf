﻿
using com.jiangjiesheng.auth;
using com.jiangjiesheng.auth.Constant;
using com.jiangjiesheng.auth.utils;
using com.jiangjiesheng.tool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.jiangjiesheng.auth
{
    /**
    * 
    * 20180429 登录用户信息读取、保存、更新等
    * 江节胜 dev@jiangjiesheng.cn
    * 
    */
    public class UserInfoCache
    {
        public const Double userValidTime = 12;
        static String AuthConfigPath = JSConfigTool.getAppDataConfigPath(JSConfigTool.DataConfigPath.AuthConfig);

        /// 
        /// 存值统一使用手机号（方便管理）
        /// 
        /// 特别注意：此服务不能用作SSO登录，因为校验了accesstoken，不需要，后期如果要做SSO，就要在客户端保存一个独立固定值key，相当于cookie值
        /// 
        /// 成功时 msg 中返回key
        /// 
        public static ServerResponse<String> setOrUpdateUserInfo(AppUserInfoEntity user)
        {
            bool isOpenAuth = Boolean.Parse(JSConfigTool.getAppDataConfig(AuthConfigPath, Keys.ConfigIsOpenAuth));
            if (!isOpenAuth)//关闭验证
            {
                return ServerResponse<String>.create(-1, "保存登陆信息失败:配置设置为不使用接口校验", "");
            }

            String accessToken = (Keys.KEY_REDIS_USER_ACCESSTOKEN_PREFIX + System.Guid.NewGuid().ToString()).ToUpper();//accessToken
            try
            {
                //存的时候使用openId取值
                //取值的时候使用使用accessToken+（手机号或者openId）校验
                if (user != null && !String.IsNullOrWhiteSpace(getAccessId(user)))
                {
                    user.AccessToken = accessToken;//用于读取时校验accessToken和accessId是否匹配

                    String userJson = JsonHelper.ObjectToJsonNotForSerialize(user);

                    String accessId = (Keys.KEY_REDIS_USER_ACCESSID_PREFIX + getAccessId(user));// accessId

                    var nowT = DateTime.Now;//UtcNow

                    String expiredConfigRedis = JSConfigTool.getAppDataConfig(AuthConfigPath, Keys.ConfigUserLoginedValidHours);
                    var expiredT = nowT.AddHours(Double.Parse(expiredConfigRedis));
                    int result = RedisCacheHelper.Add<String>(accessId, userJson, expiredT);
                    if (result == 1)
                    {
                        setCookie(getAccessId(user), accessToken, expiredConfigRedis);

                        return ServerResponse<String>.create((int)Status.SUCCESS, "保存登陆信息成功", accessToken);//成功的时候返回 reidsKey
                    }
                    return ServerResponse<String>.create((int)Status.FAIL, "保存登陆信息失败", accessToken);
                }
                return ServerResponse<String>.create((int)Status.FAIL, "保存登陆信息失败:数据异常", accessToken);

            }
            catch (Exception ex)
            {
                return ServerResponse<String>.create((int)Status.INNER_ERROR, "保存登陆信息失败:Redis服务异常,请联系管理员", accessToken);
            }

        }
        public static ServerResponse<String> updateUserInfoExpireTime(String reidsKey, TimeSpan timeSpan)//TimeSpan.FromHours(userValidTime)
        {
            try
            {
                bool res = RedisCacheHelper.Expire(reidsKey, timeSpan);
                if (res == true)
                {
                    return ServerResponse<String>.create(0, "设置成功", null);
                }
                return ServerResponse<String>.create((int)Status.FAIL, "设置失败", null);
            }
            catch (Exception ex)
            {
                return ServerResponse<String>.create((int)Status.INNER_ERROR, "设置失败:Redis服务异常,请联系管理员", null);
            }

        }

        /// <summary>
        /// String accessId, String reidsKey
        /// </summary>
        /// <param name="accessId">手机号或者openid都行</param>
        /// <param name="reidsKey"></param>
        /// <returns></returns> 
        public static ServerResponse<AppUserInfoEntity> getUserInfo()//reidsKey 外部提交
        {
            try
            {
                var accessId = RequestUtil.GetHeaderAccessId();
                var accessToken = RequestUtil.GetHeaderAccessToken();

                //20180803新增直接从cookies中获取 只上传accessToken
                var accessIdCookie = CookiesUtil.GetCookie(Keys.KEY_COOKIES_USER_ACCESSID_PREFIX);
                var accessTokenCookie = CookiesUtil.GetCookie(Keys.KEY_COOKIES_USER_ACCESSTOKEN_PREFIX);

                if ((String.IsNullOrWhiteSpace(accessToken) || String.IsNullOrWhiteSpace(accessId)) && String.IsNullOrWhiteSpace(accessTokenCookie))
                {
                    return ServerResponse<AppUserInfoEntity>.create(ResponseStatus.getCode(Status.NO_VAILD_HEADER_AND_COOKIE), "读取失败:" + ResponseStatus.getDesc(Status.NO_VAILD_HEADER_AND_COOKIE), null);
                }

                if (String.IsNullOrWhiteSpace(accessId) || String.IsNullOrWhiteSpace(accessToken)
                 || (!String.IsNullOrWhiteSpace(accessIdCookie) && !String.IsNullOrWhiteSpace(accessTokenCookie)) //优先使用Cookies
                 )
                {
                    //header头中没有获取到有效信息
                    //直接从cookie中获取
                    //且优先获取Cookies的值
                    accessId = accessIdCookie;
                    accessToken = accessTokenCookie;
                }

                String _accessId = (Keys.KEY_REDIS_USER_ACCESSID_PREFIX + accessId);// accessId

                String userJson = RedisCacheHelper.Get<String>(_accessId);//通过手机号取值

                if (String.IsNullOrWhiteSpace(userJson))
                {
                    return ServerResponse<AppUserInfoEntity>.create((int)Status.NEED_LOGIN, "用户未登录或登录失效", null);
                }
                userJson = userJson.Replace("\\r\\n", "").Replace("\\\"", "\"");
                AppUserInfoEntity user = JsonHelper.JsonToObjectNotForSerialize<AppUserInfoEntity>(userJson);
                if ((String.Equals(user.UserId, accessId) && String.Equals(user.AccessToken, accessToken)))//redis保存和接口提交的做一次校验
                {
                    //刷新有效时间
                    var nowT = DateTime.Now;//UtcNow

                    String expiredConfigRedis = JSConfigTool.getAppDataConfig(AuthConfigPath, Keys.ConfigUserLoginedValidHours);


                    var expiredT = TimeSpan.FromHours(Double.Parse(expiredConfigRedis));//更新redis
                    UserInfoCache.updateUserInfoExpireTime(_accessId, expiredT);

                    expireCookie(accessIdCookie, accessTokenCookie, expiredConfigRedis);

                    return ServerResponse<AppUserInfoEntity>.create((int)Status.SUCCESS, "读取用户缓存信息成功", user);
                }
                return ServerResponse<AppUserInfoEntity>.create((int)Status.FAIL, "参数异常", user);//redis中AccessToken已发生变化
            }
            catch (Exception ex)
            {
                return ServerResponse<AppUserInfoEntity>.create((int)Status.INNER_ERROR, "读取失败:Redis服务异常,请联系管理员", null);
            }
        }
        /// <summary>
        /// 注销登录 20180730 在之前的框架中新增的，未备份,需要同时修改RedisCacheHelper.Remove()
        /// </summary>
        /// <param name="accessId">手机号或者openid都行</param>
        /// <param name="reidsKey"></param>
        /// <returns></returns>
        public static ServerResponse<String> removeUserInfo()//reidsKey 外部提交,reidsKey是AccessToken
        {//accessId, reidsKey
            ServerResponse<AppUserInfoEntity> userQuery = getUserInfo();//这一步主要判断是不是非法注销其他账户，如果是cookie可能可以省略（cookie需要指定HttpOnly），

            if (userQuery.IsSuccess())
            {

                var accessId = RequestUtil.GetHeaderAccessId();

                //20180803新增直接从cookies中获取 只上传accessToken

                if (String.IsNullOrWhiteSpace(accessId))
                {
                    var accessIdCookie = CookiesUtil.GetCookie(Keys.KEY_COOKIES_USER_ACCESSID_PREFIX);
                    accessId = accessIdCookie;
                }
                if (String.IsNullOrWhiteSpace(accessId)) //二次校验
                {
                    return ServerResponse<String>.create((int)Status.FAIL, "注销失败(参数异常)", null);
                }
                String _accessId = (Keys.KEY_REDIS_USER_ACCESSID_PREFIX + accessId);// accessId 别忘记

                if (RedisCacheHelper.Remove(_accessId))
                {
                    removeCookie();
                    return ServerResponse<String>.create((int)Status.SUCCESS, "注销成功", null);
                }
                return ServerResponse<String>.create((int)Status.FAIL, "注销失败", null);
            }
            else if (userQuery.code == -2)
            {
                return ServerResponse<String>.create(userQuery.code, "注销失败(服务异常)", null);//redis 连接异常
            }
            return ServerResponse<String>.create(userQuery.code, "注销失败(未登录)", null); // 只剩下-1 用户未登录或登录失效
        }


        private static String getAccessId(AppUserInfoEntity user) //只取手机号UserPhone
        {
            if (user == null)
            {
                return null;
            }
            if (!ValueVarify.IsNoVaild(user.UserId))
            {
                return user.UserId;
            }

            return null;
        }

        private static void setCookie(String accessId, String accessToken, String expiredConfigRedis) // 其实只需要accessToken也行 后期改造
        {
           // 一定要同域名，最好还是别传了。默认跟web的域名，
            String domain = CommonUtil.GetDomainByRunEnv();
            CookiesUtil.SetOrUpdateCookie(Keys.KEY_COOKIES_USER_ACCESSID_PREFIX, accessId, Int32.Parse(expiredConfigRedis), null);
            CookiesUtil.SetOrUpdateCookie(Keys.KEY_COOKIES_USER_ACCESSTOKEN_PREFIX, accessToken, Int32.Parse(expiredConfigRedis), null);

        }
        private static void expireCookie(String accessId, String accessToken, String expiredConfigRedis)
        {

            String domain = CommonUtil.GetDomainByRunEnv();
            CookiesUtil.SetOrUpdateCookie(Keys.KEY_COOKIES_USER_ACCESSID_PREFIX, accessId, Int32.Parse(expiredConfigRedis), null);
            CookiesUtil.SetOrUpdateCookie(Keys.KEY_COOKIES_USER_ACCESSTOKEN_PREFIX, accessToken, Int32.Parse(expiredConfigRedis), null);
        }
        private static void removeCookie()
        {
            //CookiesUtil.RemoveCookie(Keys.KEY_COOKIES_USER_ACCESSID_PREFIX);
            //CookiesUtil.RemoveCookie(Keys.KEY_COOKIES_USER_ACCESSTOKEN_PREFIX);
            CookiesUtil.RemoveAllCookies();
        }
    }
}