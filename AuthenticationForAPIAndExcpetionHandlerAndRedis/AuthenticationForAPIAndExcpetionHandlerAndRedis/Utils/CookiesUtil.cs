﻿using com.jiangjiesheng.auth.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.jiangjiesheng.auth
{
    public class CookiesUtil
    {
        //参考 https://blog.csdn.net/jpzy520/article/details/8988901
        // https://blog.csdn.net/bjhecwq/article/details/8592401
        // https://www.cnblogs.com/w10234/p/5415946.html

        /**
        *
        *接口开发环境下Cookie如果写不进去，跟Web.config中的<add key="RunEnv" value="dev"/>有关，一定要同域名，最好还是别传了。默认跟web的域名，
        *线上的此域名应该是和web页面的域名匹配而不是API接口域名，实测是统一端口不同ip下cookie依然无法写入，
        *到时候按mm.jiangjiesheng.cn一样配置nginx动静分离
        * 
        */
        public static void SetOrUpdateCookie(String key, String value, Int32 ExpiresHour, String domain = null)
        {  //必须在AddHeader方法之前调用
            HttpCookie cookie = new HttpCookie(key, value);
            //开发环境还是判断一下环境（测试需要设置hosts文件iisconfig文件），都写入到父域名，后期如果有sso的问题，通过Header获取值吧 不要带ip否则导致整个cookie都不能写入，http都也不要加
            //java环境的话一定要加.开头，这里要不要加点不确定
            if (domain != null)
            {
                cookie.Domain = domain;//只接受父域名 "jiangjiesheng.cn"
            }
            cookie.Expires = DateTime.Now.AddHours(ExpiresHour);
            //可用 但是这是追加 HttpContext.Current.Response.Cookies.Add(cookie); //可用 
            //不能用 HttpContext.Current.Response.SetCookie();
            HttpContext.Current.Response.Cookies.Set(cookie); //以上3种写入注意 需要在AddHeader方法之前调用
        }

        public static String GetCookie(String key)
        {
            if (HttpContext.Current.Request.Cookies[key] != null)
            {
                return HttpContext.Current.Request.Cookies[key].Value;
            }
            return null;
        }

        public static void RemoveCookie(String key, String domain = null)
        {   //必须在AddHeader方法之前调用
            HttpCookie cookie = new HttpCookie(key);

            if (domain != null)
            {
                cookie.Domain = domain;//只接受父域名 "jiangjiesheng.cn"
            }

            cookie.Expires = DateTime.Now.AddHours(-1);
            HttpContext.Current.Response.Cookies.Set(cookie);
            //无效 添加进来会导致set的方法也不生效 HttpContext.Current.Response.Cookies.Remove(key);



        }
        public static void RemoveAllCookies(String domain = null)
        {
            //必须在AddHeader方法之前调用


            String[] allKeys = HttpContext.Current.Response.Cookies.AllKeys;

            foreach (String key in allKeys)
            {
                RemoveCookie(key, domain);
            }

        }
    }
}