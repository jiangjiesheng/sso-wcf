﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace com.jiangjiesheng.tool
{
    /// <summary>
    /// 值校验工具类
    /// 20171221 江节胜 dev@jiangjiesheng.cn
    /// </summary>
    public class ValueVarify
    {
        /// <summary>
        /// 是否是数字
        /// </summary>
        /// <param name="message">数字字符串</param>
        /// <returns></returns>
        public static bool isNumberic(string message)
        {
            System.Text.RegularExpressions.Regex rex =
            new System.Text.RegularExpressions.Regex(@"^\d+$");

            if (rex.IsMatch(message))
            {
                return true;
            }
            else
                return false;
        }

        //验证电话号码的主要代码如下：
        public static bool IsTelephone(string str_telephone)
        {

            return System.Text.RegularExpressions.Regex.IsMatch(str_telephone, @"^(\d{3,4}-)?\d{6,8}$");

        }

        //验证手机号码的主要代码如下：

        public static bool IsMobileNumber(string str_handset)
        {

            return System.Text.RegularExpressions.Regex.IsMatch(str_handset, @"^[1]+[3,4,5,6,7,8,9]+\d{9}");

        }

        //验证身份证号的主要代码如下：

        public static bool IsIDcard(string str_idcard)
        {

            return System.Text.RegularExpressions.Regex.IsMatch(str_idcard, @"(^\d{18}$)|(^\d{15}$)");

        }

        //验证输入为数字的主要代码如下：

        public static bool IsNumber(string str_number)
        {

            return System.Text.RegularExpressions.Regex.IsMatch(str_number, @"^[0-9]*$");

        }

        //验证邮编的主要代码如下：

        public static bool IsPostalcode(string str_postalcode)
        {

            return System.Text.RegularExpressions.Regex.IsMatch(str_postalcode, @"^\d{6}$");

        }


        public static bool IsEmail(string str_Email)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(str_Email, @"\\w{1,}@\\w{1,}\\.\\w{1,}");
        }

        /// <summary>
        /// 检查是否含有特殊字符  无效
        /// </summary>
        //public static bool checkString(string source)
        //{
        //    Regex regExp = new Regex("[~!@#$%^&*()=+[//]{}'/\";:/?.,><`|！!·￥…—（）//-、；：。，》《]");

        //    bool r = regExp.IsMatch(source);
        //    return !r;
        //}


        /// <summary>
        /// 是否只包含数字或字母 （有字母或数字即可，但不能有其他字符） 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsOnlyConatinCharOrDigital(String str)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(str, "^[0-9a-zA-Z]+$"))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 是否只包含数字和字母（字母和数字必须同时存在，且不能有其他字符） 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsOnlyConatinCharAndDigital(String str)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(str, "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9a-zA-Z]+$"))
            {
                return true;
            }
            return false;
        }
    
        /// <summary>
        /// 字符串校验，空字符串也不能通过
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNoVaild(String str)
        {
            return String.IsNullOrWhiteSpace(str);
        }
    }
}
