﻿
using com.jiangjiesheng.auth;
using com.jiangjiesheng.auth.Constant;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;

///
/// 常用工具 江节胜 
///
namespace com.jiangjiesheng.auth.utils
{
    public class CommonUtil
    {
        public static int GetEnv()
        {
            switch (GetWebConfigByKey("RunEnv"))
            {
                case "dev":
                    return (int)RunEnv.DEV;
                case "local":
                    return (int)RunEnv.LOCAL;
                case "remotetest":
                    return (int)RunEnv.REMOTE_TEST;
                case "prod":
                    return (int)RunEnv.PROD;
                default:
                    break;
            }
            return -1;
        }

        public static String GetHttpHost()
        {
            switch (GetEnv())
            {
                case (int)RunEnv.DEV:
                    return GetWebConfigByKey("Http-host-dev");
                case (int)RunEnv.LOCAL:
                    return GetWebConfigByKey("Http-host-local");
                case (int)RunEnv.REMOTE_TEST:
                    return GetWebConfigByKey("Http-host-remotetest");
                case (int)RunEnv.PROD:
                    return GetWebConfigByKey("Http-host-prod");
                default:
                    break;
            }
            return null;
        }

        public static String GetDomainByRunEnv()
        {
            switch (GetEnv())
            {
                case (int)RunEnv.DEV:
                    return GetDomainName(GetWebConfigByKey("Http-host-dev"));
                case (int)RunEnv.LOCAL:
                    return GetDomainName(GetWebConfigByKey("Http-host-local"));
                case (int)RunEnv.REMOTE_TEST:
                    return GetDomainName(GetWebConfigByKey("Http-host-remotetest"));
                case (int)RunEnv.PROD:
                    return GetDomainName(GetWebConfigByKey("Http-host-prod"));
                default:
                    break;
            }
            return null;
        }


        public static string GetDomainName(string url) //https://www.cnblogs.com/jeffwongishandsome/archive/2010/10/14/1851217.html
        {
            if (url == null)
            {
                throw new Exception("输入的url为空");
            }
            string result = string.Empty;
            string[] strArr = url.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string item in strArr)
            {
                if (string.Compare("http:", item.ToLower()) == 0)
                {
                    continue;
                }
                else if (string.Compare("https:", item.ToLower()) == 0)
                {
                    continue;
                }
                result = item;
                break;
            }
           
            int portIndex = result.IndexOf(":");
            String domain;
            if (portIndex > 0)
            {
                domain = result.Substring(0, result.IndexOf(":"));
            }
            else
            {
                domain = result;
            }
            return domain;
        }
        public static String GetWebConfigByKey(String key)
        {

            return ConfigurationManager.AppSettings[key].ToString();
        }

        public static void SendServerResponse(HttpContext context, bool isSuccess, string msg, String result)
        {
            if (context == null)
            {
                return;
            }
            ServerResponse<String> sr = ServerResponse<String>.create(isSuccess ? 0 : -1, msg, result);
            string json = ObjectToJsonForSerialize(sr);
            context.Response.Write(json);
            //context.Response.End();
        }

        #region 私方法


        private static string ObjectToJsonForSerialize<T>(T d)
        {

            DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(T));
            MemoryStream msObj = new MemoryStream();
            //将序列化之后的Json格式数据写入流中
            js.WriteObject(msObj, d);
            msObj.Position = 0;
            //从0这个位置开始读取流中的数据
            StreamReader sr = new StreamReader(msObj, Encoding.UTF8);
            string json = sr.ReadToEnd();
            sr.Close();
            msObj.Close();
            return json;
        }
        #endregion
    }
}