﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace com.jiangjiesheng.auth
{

    /**
     * 
     * 读取App_Data下自定义配置文件
     * 
     * 20180125 江节胜 dev@jiangjiesheng.cn
     * 
     */
    public class JSConfigTool
    {

        public enum DataConfigPath//枚举
        {
            WeChatOfficalAccount,//微信公众号
            SMSSender,//短信发送,
            AuthConfig//接口安全校验配置
        }
        public static String getAppDataConfigPath(DataConfigPath path)
        {

            switch (path)
            {
                case DataConfigPath.WeChatOfficalAccount:
                    return "~/App_Data/Config/WeiXin.config";

                case DataConfigPath.SMSSender:
                    return "~/App_Data/Config/SMS.config";
                case DataConfigPath.AuthConfig:
                    return "~/App_Data/Config/Auth.config";
                default:
                    return null;

            }

        }

        /// <summary>
        /// 20180125 江节胜 dev@jiangjiesheng.cn
        /// 
        /// 获取XML配置的信息
        /// </summary>
        /// <param name="xmlConfigPath"></param> ~/App_Data/Config/WeiXin.config
        /// <param name="xmlNodeName"></param> weixin/appid （注意加上父节点）
        public static String getAppDataConfig(String xmlConfigPath, String xmlNodeName)
        {
            XmlDocument Xml = new XmlDocument();
            Xml.Load(HttpContext.Current.Server.MapPath(xmlConfigPath));
            XmlNode xmlNode = Xml.SelectSingleNode(xmlNodeName);
            if (xmlNode != null)
            {
                return xmlNode.InnerText;
            }
            return null;
        }
        /// <summary>
        /// 20180125 江节胜 dev@jiangjiesheng.cn
        /// 
        /// 设置XML配置的信息
        /// </summary>
        /// <param name="xmlConfigPath"></param> ~/App_Data/Config/WeiXin.config
        /// <param name="xmlNodeName"></param> weixin/appid （注意加上父节点）
        /// <param name="value"></param>
        /// <returns></returns>
        public static Boolean setAppDataConfig(String xmlConfigPath, String xmlNodeName, String value)
        {
            XmlDocument Xml = new XmlDocument();
            Xml.Load(HttpContext.Current.Server.MapPath(xmlConfigPath));
            XmlNode xmlNode = Xml.SelectSingleNode(xmlNodeName);
            if (xmlNode != null)
            {
                xmlNode.InnerText = value;
                Xml.Save(HttpContext.Current.Server.MapPath(xmlConfigPath));
                return true;
            }

            return false;
        }
    }
}