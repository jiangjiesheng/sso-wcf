﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.jiangjiesheng.auth.utils
{
    public class RequestUtil
    {
        public static String HEADER_ACCESSTOKEN = "accesstoken";
        public static String HEADER_ACCESSID = "accessid";
        public static String GetHeaderAccessId(){
             return  HttpContext.Current.Request.Headers[HEADER_ACCESSID];
        }
        public static String GetHeaderAccessToken()
        {
            return HttpContext.Current.Request.Headers[HEADER_ACCESSTOKEN];
        }
    }
}