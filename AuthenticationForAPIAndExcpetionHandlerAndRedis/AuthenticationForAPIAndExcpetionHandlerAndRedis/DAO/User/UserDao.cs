﻿using com.jiangjiesheng.auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthenticationForAPIAndExcpetionHandlerAndRedis.DAO.User
{
    public class UserDao
    {

        public static ServerResponse<AppUserInfoEntity> GetAppUserInfoByUserId(String userId)
        {
            AppUserInfoEntity user = new AppUserInfoEntity();
            user.UserId = userId;
            return ServerResponse<AppUserInfoEntity>.create(0, "登录成功", user);
        }
    }
}