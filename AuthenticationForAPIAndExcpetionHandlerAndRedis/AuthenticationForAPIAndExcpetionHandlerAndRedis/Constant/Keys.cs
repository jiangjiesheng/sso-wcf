﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.jiangjiesheng.auth.Constant
{
    /**
   * 
   * 20180429 一般常量
   * 江节胜 dev@jiangjiesheng.cn
   * 
   */
    public class Keys
    {
        /**
         * RedisKey开始
         */
        //Redis key 注意 后期正式使用环境 要拼接 运行环境：当前系统：业务模块[可多级]：业务节点 （运行环境 需要吗?似乎没什么用）
        public const String CurrentSystemAlias = "AuthenticationForAPI:";// Redis Key 规范

        public const String KEY_REDIS_USER_ACCESSTOKEN_PREFIX = "";//使用冒号优化显示 ACCESSTOKEN: 不直接作为key存储
        public const String KEY_REDIS_USER_ACCESSID_PREFIX = CurrentSystemAlias + "ACCESSID:";//使用冒号优化显示
        /**
         * RedisKey结束
         */
        /**
        * Cookies开始
        */
        public const String KEY_COOKIES_USER_ACCESSID_PREFIX = CurrentSystemAlias + "SsoAccessId";
        public const String KEY_COOKIES_USER_ACCESSTOKEN_PREFIX = CurrentSystemAlias + "SsoAccessToken";
        /**
        /**
          * Cookies结束
          */
        public const String ConfigIsOpenAuth = "authConfig/isOpenAuth";
        public const String ConfigUserLoginedValidHours = "authConfig/userLoginedValidHours";
    }
}