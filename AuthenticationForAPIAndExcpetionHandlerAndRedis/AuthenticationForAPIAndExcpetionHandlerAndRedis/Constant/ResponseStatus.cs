﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.jiangjiesheng.auth.Constant
{
    public enum Status
    {
        SUCCESS = 0,
        FAIL = -1,
        NEED_LOGIN = -10,
        ILLEGAL_ARGUMENT = -20,
        NO_VAILD_HEADER_AND_COOKIE = -21,//无效的请求头
        ILLEGAL_REQUEST = -30,
        INNER_ERROR = -40,
        USER_ALREADY_EXIST = -50,//用户已存在
        USER_PENDING_CHECK = -60//正在等待审核
    }

    public class ResponseStatus
    {

        /// <summary>
        /// ResponseStatus.getCode(Status.SUCCESS)
        /// 20180130 江节胜 dev@jiangjiesheng.cn
        /// </summary>

        public static int getCode(Status status)//获取描述
        {
            return (int)status;//可以直接int强制转

            //int result = -1000;
            //switch (status)
            //{
            //    case Status.SUCCESS:
            //        result = 0;
            //        break;
            //    case Status.FAIL:
            //        result =-1;
            //        break;
            //    case Status.NEED_LOGIN:
            //        result = -10;
            //        break;
            //    case Status.ILLEGAL_ARGUMENT:
            //        result = -20;
            //        break;
            //    case Status.NO_VAILD_HEADER_AND_COOKIE:
            //          result = -21;
            //        break;
            //    case Status.ILLEGAL_REQUEST:
            //        result = -30;
            //        break;
            //    case Status.INNER_ERROR:
            //        result = -40;
            //        break;
            //    case Status.USER_ALREADY_EXIST:
            //        result = -50;
            //        break;
            //    case Status.USER_PENDING_CHECK:
            //        result = -60;
            //        break; 
            //    default:
            //        result = -1000;
            //        break;
            //}
            // return result;
        }
        /// <summary>
        /// ResponseStatus.getDesc(Status.SUCCESS)
        /// 20180130 江节胜 dev@jiangjiesheng.cn
        /// </summary>
        public static string getDesc(Status status)//获取描述
        {
            string result = string.Empty;
            switch (status)
            {
                case Status.SUCCESS:
                    result = "请求成功";
                    break;
                case Status.FAIL:
                    result = "请求失败";
                    break;
                case Status.NEED_LOGIN:
                    result = "未登录";
                    break;
                case Status.ILLEGAL_ARGUMENT:
                    result = "非法参数";
                    break;
                case Status.NO_VAILD_HEADER_AND_COOKIE:
                    result = "无请求头且无Cookies";
                    break;
                case Status.ILLEGAL_REQUEST:
                    result = "非法请求";
                    break;
                case Status.INNER_ERROR:
                    result = "内部错误";
                    break;
                case Status.USER_ALREADY_EXIST:
                    result = "用户已存在";
                    break;
                case Status.USER_PENDING_CHECK:
                    result = "正在等待审核";
                    break;
                default:
                    result = "未知错误";
                    break;
            }
            return result;
        }
    }
}
