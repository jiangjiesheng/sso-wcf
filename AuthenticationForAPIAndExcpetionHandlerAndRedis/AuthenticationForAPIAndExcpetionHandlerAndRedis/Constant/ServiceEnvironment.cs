﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;

namespace com.jiangjiesheng.auth.Constant
{
    public class ServiceEnvironment
    {
        public const string ServiceNamespace = "com.aotu.jianantong";
        /// webjson请求格式
        /// </summary>
        public const WebMessageFormat WebRequestFormatJson = WebMessageFormat.Json;

        /// <summary>
        /// webjson相应格式
        /// </summary>
        public const WebMessageFormat WebResponseFormatJson = WebMessageFormat.Json;

        /// webxml请求格式
        /// </summary>
        public const WebMessageFormat WebRequestFormatXML = WebMessageFormat.Xml;

        /// <summary>
        /// web相应格式
        /// </summary>
        public const WebMessageFormat WebResponseFormatXML = WebMessageFormat.Xml;
        /// <summary>
        /// 请求内容封装方式
        /// </summary>
        public const WebMessageBodyStyle WebBodyStyleBare = WebMessageBodyStyle.Bare;

        public const WebMessageBodyStyle WebBodyStyleWrapped = WebMessageBodyStyle.Wrapped;
    }
}