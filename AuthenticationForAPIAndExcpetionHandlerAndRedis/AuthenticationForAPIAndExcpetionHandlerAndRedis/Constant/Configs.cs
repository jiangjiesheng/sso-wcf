﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace com.jiangjiesheng.auth.Constant
{
    public class Configs
    {
        static String configPath = JSConfigTool.getAppDataConfigPath(JSConfigTool.DataConfigPath.WeChatOfficalAccount);


        public static String GetWxAppId()
        {
               bool is_release = IsReleaseEnv();
               if (is_release)
               {
                   return JSConfigTool.getAppDataConfig(configPath, "weixin/appid-prod");
               }
               return JSConfigTool.getAppDataConfig(configPath, "weixin/appid-dev"); 
        }

        public static String GetWxAppSecret()
        {
            bool is_release = IsReleaseEnv();
            if (is_release)
            {
                return JSConfigTool.getAppDataConfig(configPath, "weixin/appsecret-prod");
            }
            return JSConfigTool.getAppDataConfig(configPath, "weixin/appsecret-dev");
        }
        public static String GetWxAccessToken()
        {

            bool is_release = IsReleaseEnv();
            if (is_release)
            {
                return JSConfigTool.getAppDataConfig(configPath, "weixin/accesstoken-prod");
            }
            return JSConfigTool.getAppDataConfig(configPath, "weixin/accesstoken-dev");
        }

        public static void SetWxAccessToken(String AccessToken)
        {
            bool is_release = IsReleaseEnv();
            if (is_release)
            {
                JSConfigTool.setAppDataConfig(configPath, "weixin/accesstoken-prod", AccessToken);
            }
            else
            {
                JSConfigTool.setAppDataConfig(configPath, "weixin/accesstoken-dev", AccessToken);
            }
        }

        public static String GetWxToken()
        {
            return JSConfigTool.getAppDataConfig(configPath, "weixin/callback-token");
        }

        /// <summary>
        /// 20180125 江节胜 dev@jiangjiesheng.cn
        /// 根据配置来组装host 
        ///  --> http://wxgzhapitest.gesturectrl.com/
        ///  --> http://wxgzhapi.aotusoft.cn/
        /// </summary>
        /// <returns></returns>
        public static String GetWeChatCallBackHost()
        {
            //bool is_release = JSConfigTool.getAppDataConfig(configPath, "weixin/is_release").Equals("true") ? true : false;//不要写成静态变量
            bool is_release = IsReleaseEnv();
            String host;
            if (is_release)
            {
                host = JSConfigTool.getAppDataConfig(configPath, "weixin/host/release-host").Replace("\r", "").Replace("\n", "").Replace(" ", "");
                return host;
            }
            host = JSConfigTool.getAppDataConfig(configPath, "weixin/host/dev-host").Replace("\r", "").Replace("\n", "").Replace(" ", "");
            return host;
        }



        /// <summary>
        /// 20180125 江节胜 dev@jiangjiesheng.cn
        /// 短信发送
        ///  --> http://wxgzhapitest.gesturectrl.com/
        ///  --> http://wxgzhapi.aotusoft.cn/
        /// </summary>
        /// <returns></returns>
        public static String GetSMSSenderHost()
        {
            String SMSSenderConfigPath = JSConfigTool.getAppDataConfigPath(JSConfigTool.DataConfigPath.SMSSender);
            //bool is_release = JSConfigTool.getAppDataConfig(configPath, "weixin/is_release").Equals("true") ? true : false;//不要写成静态变量
            bool is_release = IsReleaseEnv();
            if (is_release)
            {
                return JSConfigTool.getAppDataConfig(SMSSenderConfigPath, "sms/host_format").Replace("\r", "").Replace("\n", "").Replace(" ", "");
            }
            return JSConfigTool.getAppDataConfig(SMSSenderConfigPath, "sms/host_remote_test").Replace("\r", "").Replace("\n", "").Replace(" ", "");
        }
        
        private static Boolean IsReleaseEnv()
        {
            return JSConfigTool.getAppDataConfig(configPath, "weixin/is_release").Equals("true") ? true : false;//不要写成静态变量
        }
    }


}