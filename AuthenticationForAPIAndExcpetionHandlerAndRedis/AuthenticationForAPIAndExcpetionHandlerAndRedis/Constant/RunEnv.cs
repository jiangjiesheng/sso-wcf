﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.jiangjiesheng.auth.Constant
{
    public enum RunEnv
    {
        DEV = -1,//开发者本机
        LOCAL = 0,
        REMOTE_TEST = 1,
        PROD=2 //生产
    }
}