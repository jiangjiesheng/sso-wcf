﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Web;

namespace com.jiangjiesheng.auth.Exceptions
{
    public class WCF_ExceptionHandler : IErrorHandler
    {
        #region IErrorHandler Members

        /// <summary> 
        /// HandleError 
        /// </summary> 
        /// <param name="ex">ex</param> 
        /// <returns>true</returns> 
        public bool HandleError(Exception ex)
        {
            return true;
        }

        /// <summary> 
        /// ProvideFault 
        /// </summary> 
        /// <param name="ex">ex</param> 
        /// <param name="version">version</param> 
        /// <param name="msg">msg</param> 
        public void ProvideFault(Exception ex, MessageVersion version, ref Message msg)
        {
            // 
            //在这里处理服务端的消息，将消息写入服务端的日志 
            // 
            string err = string.Format("自定义WCF接口全局异常捕获： '{0}' 出错", ex.TargetSite.Name) + "，详情：\r\n" + ex.Message;
            var newEx = new FaultException(err);

            MessageFault msgFault = newEx.CreateMessageFault();
            msg = Message.CreateMessage(version, msgFault, newEx.Action);
            msg = Message.CreateMessage(version, "-1000", err);// 抛出一些异常 180730江节胜
        }

        #endregion
    }
}
